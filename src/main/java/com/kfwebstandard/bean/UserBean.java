package com.kfwebstandard.bean;

import java.io.Serializable;

import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Named("user")
@SessionScoped
public class UserBean implements Serializable {

    private final static Logger LOG = LoggerFactory.getLogger(UserBean.class);

    private String name;
    private String password;
    private String aboutYourself;

    public String getName() {
        LOG.debug("UserBean: getName");
        return name;
    }

    public void setName(String newValue) {
        LOG.debug("UserBean: setName");
        name = newValue;
    }

    public String getPassword() {
        LOG.debug("UserBean: getPassword");
        return password;
    }

    public void setPassword(String newValue) {
        LOG.debug("UserBean: setPassword");
        password = newValue;
    }

    public String getAboutYourself() {
        LOG.debug("UserBean: getAboutyourself");
        return aboutYourself;
    }

    public void setAboutYourself(String newValue) {
        LOG.debug("UserBean: setAboutyourself");
        aboutYourself = newValue;
    }
}
