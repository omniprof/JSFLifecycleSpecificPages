package com.kfwebstandard.commands;

import java.io.Serializable;
import java.util.Locale;

import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import javax.faces.context.FacesContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Named
@SessionScoped
public class LocaleChanger implements Serializable {

    private final static Logger LOG = LoggerFactory.getLogger(LocaleChanger.class);

    public String frenchAction() {
        LOG.debug("frenchAction");

        FacesContext context = FacesContext.getCurrentInstance();
        context.getViewRoot().setLocale(Locale.CANADA_FRENCH);
        return null;
    }

    public String englishAction() {
        LOG.debug("englishAction");

        FacesContext context = FacesContext.getCurrentInstance();
        context.getViewRoot().setLocale(Locale.CANADA);
        return null;
    }
}
