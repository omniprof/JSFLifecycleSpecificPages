package com.kfwebstandard.listener;

import javax.faces.event.PhaseEvent;
import javax.faces.event.PhaseId;
import javax.faces.event.PhaseListener;
import javax.inject.Named;
import javax.enterprise.context.RequestScoped;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This class will intercept every JSF phase and display the phase ID
 *
 * @author Ken Fogel
 */
@Named
@RequestScoped
public class LifeCycleListener implements PhaseListener {

    private final static Logger LOG = LoggerFactory.getLogger(LifeCycleListener.class);

    @Override
    public PhaseId getPhaseId() {
        return PhaseId.ANY_PHASE;
    }

    @Override
    public void beforePhase(PhaseEvent event) {
        LOG.info("-----------------------------------");
        LOG.info("Start Phase: " + event.getPhaseId());
    }

    @Override
    public void afterPhase(PhaseEvent event) {
        LOG.info("End Phase: " + event.getPhaseId());
    }

}
